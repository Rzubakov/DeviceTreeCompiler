package EJB;

import javax.mail.MessagingException;

public interface MailInt {

    public void send(String addresses, String topic, String textMessage) throws MessagingException;
}
