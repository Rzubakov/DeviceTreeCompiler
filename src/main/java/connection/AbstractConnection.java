package connection;

import java.util.UUID;

public class AbstractConnection implements ConnectionInt {

    protected UUID id;
    protected Integer asdualen;
    protected Integer ioalen;
    protected Integer cotlen;
    protected Integer t0;
    protected Integer t1;
    protected Integer t2;
    protected Integer t3;
    protected Integer k;
    protected Integer w;
    protected Integer maxclients;
    protected Integer reanimatetime;
    protected Integer ctime;
    protected Integer yearstart;

    @Override
    public UUID getId() {
        return id;
    }

    @Override
    public void setId(UUID id) {
        this.id = id;
    }

    @Override
    public Integer getAsdualen() {
        return this.asdualen;
    }

    @Override
    public void setAsdualen(Integer asdualen) {
        this.asdualen = asdualen;
    }

    @Override
    public Integer getIoalen() {
        return this.ioalen;
    }

    @Override
    public void setIoalen(Integer ioalen) {
        this.ioalen = ioalen;
    }

    @Override
    public Integer getCotlen() {
        return this.cotlen;
    }

    @Override
    public void setCotlen(Integer cotlen) {
        this.cotlen = cotlen;
    }

    @Override
    public Integer getK() {
        return this.k;
    }

    @Override
    public void setK(Integer k) {
        this.k = k;
    }
 
    @Override
    public Integer getT0() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void setT0(Integer t0) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Integer getT1() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void setT1(Integer t1) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Integer getT2() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void setT2(Integer t2) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Integer getT3() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void setT3(Integer t3) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Integer getW() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void setW(Integer w) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Integer getMaxclients() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void setMaxclients(Integer maxclients) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Integer getReanimatetime() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void setReanimatetime(Integer reanimatetime) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Integer getCtime() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void setCtime(Integer ctime) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Integer getYearstart() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void setYearstart(Integer yearstart) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

}
