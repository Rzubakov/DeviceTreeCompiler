package connection;

import java.util.UUID;

public interface ConnectionInt {

    public UUID getId();

    public void setId(UUID id);

    public Integer getAsdualen();

    public void setAsdualen(Integer asdualen);

    public Integer getIoalen();

    public void setIoalen(Integer ioalen);

    public Integer getCotlen();

    public void setCotlen(Integer cotlen);

    public Integer getT0();

    public void setT0(Integer t0);

    public Integer getT1();

    public void setT1(Integer t1);

    public Integer getT2();

    public void setT2(Integer t2);

    public Integer getT3();

    public void setT3(Integer t3);

    public Integer getK();

    public void setK(Integer k);

    public Integer getW();

    public void setW(Integer w);

    public Integer getMaxclients();

    public void setMaxclients(Integer maxclients);

    public Integer getReanimatetime();

    public void setReanimatetime(Integer reanimatetime);

    public Integer getCtime();

    public void setCtime(Integer ctime);

    public Integer getYearstart();

    public void setYearstart(Integer yearstart);

}
