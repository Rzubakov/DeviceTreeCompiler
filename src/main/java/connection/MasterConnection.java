package connection;

public class MasterConnection extends AbstractConnection {

    @Override
    public Integer getT0() {
        return this.t0;
    }

    @Override
    public void setT0(Integer t0) {
        this.t0 = t0;
    }

    @Override
    public Integer getT1() {
        return this.t1;
    }

    @Override
    public void setT1(Integer t1) {
        this.t1 = t1;
    }

    @Override
    public Integer getT2() {
        return this.t2;
    }

    @Override
    public void setT2(Integer t2) {
        this.t2 = t2;
    }

    @Override
    public Integer getT3() {
        return this.t3;
    }

    @Override
    public void setT3(Integer t3) {
        this.t3 = t3;
    }

    @Override
    public Integer getW() {
        return this.w;
    }

    @Override
    public void setW(Integer w) {
        this.w = w;
    }
}
