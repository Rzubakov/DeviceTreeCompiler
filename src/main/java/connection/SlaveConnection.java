package connection;

public class SlaveConnection extends AbstractConnection {

    @Override
    public Integer getMaxclients() {
        return this.maxclients;
    }

    @Override
    public void setMaxclients(Integer maxclients) {
        this.maxclients = maxclients;
    }

    @Override
    public Integer getReanimatetime() {
        return this.reanimatetime;
    }

    @Override
    public void setReanimatetime(Integer reanimatetime) {
        this.reanimatetime = reanimatetime;
    }

    @Override
    public Integer getCtime() {
        return this.ctime;
    }

    @Override
    public void setCtime(Integer ctime) {
        this.ctime = ctime;
    }

    @Override
    public Integer getYearstart() {
        return this.yearstart;
    }

    @Override
    public void setYearstart(Integer yearstart) {
        this.yearstart = yearstart;
    }
}
