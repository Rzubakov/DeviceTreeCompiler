package controller;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.UUID;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import module.ModuleInt;

public abstract class AbstractController implements ControllerInt {

    @NotNull(message = "ID обязателен")
    protected UUID id;
    protected List<ModuleInt> modules = new ArrayList<>();
    @Size(min = 5, max = 255, message = "Наименование от 5 до 255 символов")
    protected String name;
    @Min(value = 8, message = "Посадочных мест не должно быть меньше 8")
    @Max(value = 32, message = "Посадочных мест не должно быть больше 32")
    protected Integer countUnit;
    @Min(value = 1, message = "Кол-во ЦП не должно быть меньше 1")
    @Max(value = 2, message = "Кол-во ЦП не должно быть больше 2")
    protected Integer countCpu;

    public AbstractController() {
    }

    @Override
    public UUID getId() {
        return id;
    }

    @Override
    public void setId(UUID id) {
        this.id = id;
    }

    @Override
    public void addModule(ModuleInt module) {

        modules.add(module);
    }

    @Override
    public ModuleInt getModule(int index) {
        return modules.get(index);
    }

    @Override
    public List<ModuleInt> getModules() {
        return modules;
    }

    @Override
    public void setModules(List<ModuleInt> modules) {
        this.modules = modules;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public void setName(String name) {
        this.name = name;
    }

    @Override
    public ModuleInt find(ModuleInt module) {
        return modules.get(modules.indexOf(module));
    }

    @Override
    public void deleteModule(ModuleInt module) {

        modules.remove(modules.indexOf(module));
    }

    @Override
    public Integer getCountUnit() {
        return countUnit;
    }

    @Override
    public void setCountUnit(Integer countUnit) {
        this.countUnit = countUnit;
    }

    @Override
    public Integer getCountCpu() {
        return countCpu;
    }

    @Override
    public void setCountCpu(Integer countCpu) {
        this.countCpu = countCpu;
    }

    @Override
    public Boolean isFull() {
        return countCpu + modules.size() >= countUnit;
    }

    @Override
    public Integer emptyCount() {
        return countUnit - (modules.size() + countCpu);
    }

    @Override
    public String toString() {
        return this.getClass().getSimpleName();
    }

    @Override
    public List<String> getConfig() {
        List<String> config = new ArrayList<>();
        config.add("/*АО Текон-Инжиниринг*/");
        config.add("/*Ведущий инженер-программист: Зубаков Р.А.*/");
        config.add("/*Проект: " + name + "*/");
        config.add("/*" + Calendar.getInstance().getTime().toString() + "*/");
        config.add("/dts-v1/;");
        config.add("/{");
        config.add("ubus{");
        config.add("version = \"1.0.0\";");
        modules.forEach((module) -> {
            module.getConfig().forEach(cfg -> {
                config.add(cfg);
            });
        });
        config.add("};");
        config.add("};");
        return config;
    }

}
