package controller;

import java.util.List;
import java.util.UUID;
import module.ModuleInt;

public interface ControllerInt {

    public UUID getId();

    public void setId(UUID id);

    public void addModule(ModuleInt module);

    public void deleteModule(ModuleInt module);

    public ModuleInt getModule(int index);

    public List<ModuleInt> getModules();

    public void setModules(List<ModuleInt> modules);

    public String getName();

    public void setName(String name);

    public ModuleInt find(ModuleInt module);

    public Integer getCountUnit();

    public void setCountUnit(Integer countUnit);

    public Integer getCountCpu();

    public void setCountCpu(Integer countCpu);

    public List<String> getConfig();

    public Boolean isFull();

    public Integer emptyCount();

}
