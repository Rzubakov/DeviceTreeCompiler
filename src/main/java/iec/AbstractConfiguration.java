package iec;

import connection.ConnectionInt;

public class AbstractConfiguration implements IecConfiguration {

    protected ConnectionInt connection;

    @Override
    public ConnectionInt getConnection() {
        return this.connection;
    }

    @Override
    public void setConnection(ConnectionInt connection) {
        this.connection = connection;
    }

}
