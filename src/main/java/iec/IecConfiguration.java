package iec;

import connection.ConnectionInt;


public interface IecConfiguration {

    public ConnectionInt getConnection();

    public void setConnection(ConnectionInt connection);

}
