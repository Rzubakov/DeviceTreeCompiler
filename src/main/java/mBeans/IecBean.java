package mBeans;

import controller.ControllerInt;
import java.io.Serializable;
import java.util.List;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import project.ProjectInt;

@ManagedBean(name = "iecBean")
@ViewScoped
public class IecBean implements Serializable {

    private static final long serialVersionUID = 5252679240689817344L;

    @ManagedProperty("#{sessionBean}")
    private SessionBean session;
    private ProjectInt selectedProject;
    private ControllerInt selectedController;

    public IecBean() {
    }

    public SessionBean getSession() {
        return session;
    }

    public void setSession(SessionBean session) {
        this.session = session;
    }

    public List<ProjectInt> getProjects() {
        return session.getProjects();
    }

    public ProjectInt getSelectedProject() {
        return selectedProject;
    }

    public void setSelectedProject(ProjectInt selectedProject) {
        this.selectedProject = selectedProject;
    }

    public ControllerInt getSelectedController() {
        return selectedController;
    }

    public void setSelectedController(ControllerInt selectedController) {
        this.selectedController = selectedController;
    }

}
