package mBeans;

import EJB.MailInt;
import java.io.Serializable;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.mail.MessagingException;

@ManagedBean(name = "requestBean")
@ViewScoped
public class RequestBean implements Serializable {

    private static final long serialVersionUID = 4027279596146681638L;

    @EJB
    private MailInt mailService;
    private String email;
    private String firstname;
    private String lastname;

    public RequestBean() {
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public void sendRequest() {
        try {
            mailService.send("rzubakov@tecon.ru", "RequestRegistration", "Запрос от пользователя:\n" + email + "\n" + firstname + "\n" + lastname + "\n");
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Запрос отправлен.", "Доступ будет предоставлен в ближайшее время."));
        } catch (MessagingException e) {
            System.out.println(e);
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Ошибка отправки.!", "Свяжитесь с администратором."));
        }

    }
}
