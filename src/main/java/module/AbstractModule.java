package module;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import chanel.ChanelInt;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

public abstract class AbstractModule implements ModuleInt {

    @NotNull(message = "ID обязателен")
    protected UUID id;
    @Min(value = 8, message = "Посадочное место не должно быть меньше 1")
    @Max(value = 32, message = "Посадочное место не должно быть больше 32")
    protected Integer index;
    protected String vname;
    protected String ts_vname;
    protected String kind;
    protected Integer size;
    private List<ChanelInt> chanels = new ArrayList<>();

    public AbstractModule() {
    }

    @Override
    public UUID getId() {
        return id;
    }

    @Override
    public void setId(UUID id) {
        this.id = id;
    }

    @Override
    public void addChanel(ChanelInt chanel) {
        chanels.add(chanel);
    }

    @Override
    public ChanelInt getChanel(int index) {
        return chanels.get(index);
    }

    @Override
    public Integer getIndex() {
        return this.index;
    }

    @Override
    public void setIndex(Integer index) {
        this.index = index;
    }

    @Override
    public String getVname() {
        return this.vname;
    }

    @Override
    public void setVname(String vname) {
        this.vname = vname;
    }

    @Override
    public String getTs_vname() {
        return this.ts_vname;
    }

    @Override
    public void setTs_vname(String ts_vname) {
        this.ts_vname = ts_vname;
    }

    @Override
    public Integer getSize() {
        return size;
    }

    @Override
    public void setSize(Integer size) {
        this.size = size;
    }

    @Override
    public List<ChanelInt> getChanels() {
        return chanels;
    }

    @Override
    public void setChanels(List<ChanelInt> chanels) {
        this.chanels = chanels;
    }

    @Override
    public String getKind() {
        return kind;
    }

    @Override
    public void setKind(String kind) {
        this.kind = kind;
    }

    @Override
    public Boolean isFull() {
        return chanels.size() >= size;
    }

    @Override
    public Boolean isSingleFull() {
        return chanels.size() + 1 > size;
    }

    @Override
    public Boolean isDoubleFull() {
        return chanels.size() + 2 > size;
    }

    @Override
    public List<String> getConfig() {
        List<String> config = new ArrayList<>();
        config.add("DI32@" + id.toString() + "{");
        config.add("vname=\"" + vname + "\";");
        config.add("ts_vname=\"" + ts_vname + "\";");
        config.add("kind=\"" + kind + "\";");
        chanels.forEach((chanel) -> {
            config.add(chanel.getConfig());
        });
        config.add("};");
        return config;
    }

    @Override
    public String toString() {
        return this.getClass().getSimpleName();
    }
}
