package module;

public class DI32 extends AbstractModule {

    public DI32() {
        this.index = 0;
        this.size = 32;
        this.ts_vname = "DI32";
        this.vname = "DI32/i";
        this.kind = "<0x0>";
    }
}
