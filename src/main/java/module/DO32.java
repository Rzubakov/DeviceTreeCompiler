package module;

public class DO32 extends AbstractModule {

    public DO32() {
        this.index = 0;
        this.size = 32;
        this.ts_vname = "DO32";
        this.vname = "DO32/o";
        this.kind = "<0x0>";
    }
}
