package project;

import java.util.List;
import controller.ControllerInt;
import java.util.ArrayList;
import java.util.UUID;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

public abstract class AbstractProject implements ProjectInt {

    @NotNull(message = "ID обязателен")
    protected UUID id;
    @Size(min = 5, max = 255, message = "Наименование проекта от 5 до 255 символов")
    protected String name;

    protected List<ControllerInt> controllers = new ArrayList<>();

    @Override
    public UUID getId() {
        return id;
    }

    @Override
    public void setId(UUID id) {
        this.id = id;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public void setName(String name) {
        this.name = name;
    }

    @Override
    public List<ControllerInt> getControllers() {
        return controllers;
    }

    @Override
    public void setControllers(List<ControllerInt> controllers) {
        this.controllers = controllers;
    }

    @Override
    public void addController(ControllerInt controller) {
        controllers.add(controller);
    }

    @Override
    public void deleteController(ControllerInt controller) {
        controllers.remove(controller);
    }

}
